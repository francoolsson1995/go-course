CREATE TABLE audits (
    id bigserial,
    user_id VARCHAR(255),
    json_info VARCHAR (23767) constraint ensure_json check (json_info::jsonb is not null),
    PRIMARY KEY (id)
    )
