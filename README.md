# Bankuish Test

This repository has the code for Bankuish test.

## Installation

To test this app in local you must:

* Install [Golang 1.17+](https://golang.org/doc/install)
* Install [Docker](https://docs.docker.com/get-docker/)
* Install [Docker Compose](https://docs.docker.com/compose/install/)
* Install [Postman](https://www.postman.com/downloads/) (Optional)

## Usage

To build and turn on the app you must:

* Clone this repository in your local machine.
* Go to the root of the project.
* Run `docker-compose up -d` to turn on the database.
* Run `go run cmd/api/main.go` to turn on the app in local.
* Import the `test.postman_collection.json` file in Postman to test the endpoints.

## Issues

* This app does not have integration/e2c tests.
* The user validation service is mocked, and the only user and password that works is `admin` and `admin`.
* In auth endpoint, the token generated is a firebase custom token; but the necessary token to access
  the other endpoints is a firebase id token. This is because this app does not have Firebase SDK. By now,
  if you want to test courses endpoints, you must use the "Firebase (it should be SDK) - Custom token to ID token"
  postman request to get the id token from the custom token generated in auth endpoint.
