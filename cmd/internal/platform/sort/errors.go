package sort

import (
	"fmt"
)

// ErrCyclicObserver is an error that is returned when it is detected that, during the loading of a node,
// all the nodes are cyclically traversed, which means that the ordering in levels of the nodes is impossible.
type ErrCyclicObserver struct {
	detail string
}

func newErrCyclicObserver(detail string) *ErrCyclicObserver {
	return &ErrCyclicObserver{detail: detail}
}

// Error allows ErrCycleObserver implements error interface.
func (ec *ErrCyclicObserver) Error() string {
	return fmt.Sprintf("Cyclic observer is true in: %s", ec.detail)
}

// Is method allows ErrCyclicObserver implements itself errors assertions.
func (ec *ErrCyclicObserver) Is(err error) bool {
	if castedError, ok := err.(*ErrCyclicObserver); ok {
		{
			return castedError.detail == ec.detail
		}
	}
	return false
}
