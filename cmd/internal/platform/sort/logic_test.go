package sort

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReturnOrderedNames(t *testing.T) {
	const (
		physicsTwo     = "Physics two"
		physicsOne     = "Physics one"
		chemistry      = "Chemistry"
		biology        = "Biology"
		thermodynamics = "Thermodynamics"
		calculusOne    = "Calculus one"
		calculusTwo    = "Calculus two"
		naturalScience = "Natural Science"
		algebra        = "Algebra"

		portfolioConstruction = "PortfolioConstruction"
		portfolioTheories     = "PortfolioTheories"
		investmentManagement  = "InvestmentManagement"
		investment            = "Investment"
		finance               = "finance"
		investmentStyle       = "InvestmentStyle"
	)

	type args struct {
		pairedNames []PairedNames
	}

	tests := []struct {
		name                string
		args                args
		wantSortedNamesInfo []SortedNamesInfo
		wantError           error
	}{
		{
			name: "Happy Path - all nodes must be ordered without errors",
			args: args{
				pairedNames: []PairedNames{
					{
						SubscriberNodeName: physicsTwo,
						NotifierNodeName:   algebra,
					},
					{
						SubscriberNodeName: physicsTwo,
						NotifierNodeName:   physicsOne,
					},
					{
						SubscriberNodeName: thermodynamics,
						NotifierNodeName:   chemistry,
					},
					{
						SubscriberNodeName: calculusTwo,
						NotifierNodeName:   calculusOne,
					},
					{
						SubscriberNodeName: biology,
						NotifierNodeName:   naturalScience,
					},
					{
						SubscriberNodeName: thermodynamics,
						NotifierNodeName:   physicsTwo,
					},
					{
						SubscriberNodeName: chemistry,
						NotifierNodeName:   biology,
					},
				},
			},
			wantSortedNamesInfo: []SortedNamesInfo{
				{
					Name:  algebra,
					Level: 0,
				},
				{
					Name:  physicsOne,
					Level: 0,
				},
				{
					Name:  calculusOne,
					Level: 0,
				},
				{
					Name:  naturalScience,
					Level: 0,
				},
				{
					Name:  physicsTwo,
					Level: 1,
				},
				{
					Name:  biology,
					Level: 1,
				},
				{
					Name:  calculusTwo,
					Level: 1,
				},
				{
					Name:  chemistry,
					Level: 2,
				},
				{
					Name:  thermodynamics,
					Level: 3,
				},
			},
		},
		{
			name: "Fail Path - cyclic observer throws errors because thermodynamics is dependant than natural science," +
				" but natural science is dependant than thermodynamics (natural science - biology - chemistry -" +
				" thermodynamics)",
			args: args{
				pairedNames: []PairedNames{
					{
						SubscriberNodeName: physicsTwo,
						NotifierNodeName:   algebra,
					},
					{
						SubscriberNodeName: physicsTwo,
						NotifierNodeName:   physicsOne,
					},
					{
						SubscriberNodeName: thermodynamics,
						NotifierNodeName:   chemistry,
					},
					{
						SubscriberNodeName: calculusTwo,
						NotifierNodeName:   calculusOne,
					},
					{
						SubscriberNodeName: biology,
						NotifierNodeName:   naturalScience,
					},
					{
						SubscriberNodeName: thermodynamics,
						NotifierNodeName:   physicsTwo,
					},
					{
						SubscriberNodeName: chemistry,
						NotifierNodeName:   biology,
					},
					{
						SubscriberNodeName: naturalScience,
						NotifierNodeName:   thermodynamics,
					},
				},
			},
			wantError: newErrCyclicObserver(thermodynamics),
		},
		{
			name: "Happy path - all nodes must be ordered without errors, following the test case from the challenge",
			args: args{
				pairedNames: []PairedNames{
					{
						SubscriberNodeName: portfolioConstruction,
						NotifierNodeName:   portfolioTheories,
					},
					{
						SubscriberNodeName: investmentManagement,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investment,
						NotifierNodeName:   finance,
					},
					{
						SubscriberNodeName: portfolioTheories,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investmentStyle,
						NotifierNodeName:   investmentManagement,
					},
				},
			},
			wantSortedNamesInfo: []SortedNamesInfo{
				{
					Name:  finance,
					Level: 0,
				},
				{
					Name:  investment,
					Level: 1,
				},
				{
					Name:  investmentManagement,
					Level: 2,
				},
				{
					Name:  portfolioTheories,
					Level: 2,
				},
				{
					Name:  investmentStyle,
					Level: 3,
				},
				{
					Name:  portfolioConstruction,
					Level: 3,
				},
			},
		},
		{
			name: "Happy path - all nodes must be ordered without errors, following the test case from the challenge," +
				"and adding a new node that not introduce dependency (portfolioConstruction from investment, that is a" +
				"implicit dependency already declared) and this should not modify the order of the nodes of the " +
				"latest test case",
			args: args{
				pairedNames: []PairedNames{
					{
						SubscriberNodeName: portfolioConstruction,
						NotifierNodeName:   portfolioTheories,
					},
					{
						SubscriberNodeName: investmentManagement,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investment,
						NotifierNodeName:   finance,
					},
					{
						SubscriberNodeName: portfolioTheories,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investmentStyle,
						NotifierNodeName:   investmentManagement,
					},
					{
						SubscriberNodeName: portfolioConstruction,
						NotifierNodeName:   investment,
					},
				},
			},
			wantSortedNamesInfo: []SortedNamesInfo{
				{
					Name:  finance,
					Level: 0,
				},
				{
					Name:  investment,
					Level: 1,
				},
				{
					Name:  investmentManagement,
					Level: 2,
				},
				{
					Name:  portfolioTheories,
					Level: 2,
				},
				{
					Name:  investmentStyle,
					Level: 3,
				},
				{
					Name:  portfolioConstruction,
					Level: 3,
				},
			},
		},
		{
			name: "Fail path - add a recursive dependency that should return an error (investment from investment)",
			args: args{
				pairedNames: []PairedNames{
					{
						SubscriberNodeName: portfolioConstruction,
						NotifierNodeName:   portfolioTheories,
					},
					{
						SubscriberNodeName: investmentManagement,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investment,
						NotifierNodeName:   finance,
					},
					{
						SubscriberNodeName: portfolioTheories,
						NotifierNodeName:   investment,
					},
					{
						SubscriberNodeName: investmentStyle,
						NotifierNodeName:   investmentManagement,
					},
					{
						SubscriberNodeName: investment,
						NotifierNodeName:   investment,
					},
				},
			},
			wantError: newErrCyclicObserver(investment),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sorterService := NewSorterService()
			results, err := sorterService.ReturnOrderedNames(Names{
				PairedNames: tt.args.pairedNames,
			})
			for _, resultNamesInfo := range results.NamesInfo {
				for _, wantNameInfo := range tt.wantSortedNamesInfo {
					if resultNamesInfo.Name == wantNameInfo.Name {
						require.Equal(t, wantNameInfo.Level, resultNamesInfo.Level)
					}
				}
			}
			require.ErrorIs(t, err, tt.wantError)
		})
	}
}
