package sort

import "sort"

// Sorter represents the service that sorts the nodes.
type Sorter struct{}

// NewSorterService returns a new instance of Sorter.
func NewSorterService() *Sorter {
	return &Sorter{}
}

// ReturnOrderedNames returns the ordered names of the nodes.
// This function receives a Names struct that contains a slice of PairedNames.
// The PairedNames represents a pair of names that are related to each other.
// Then, by each pair of related names, the service the nodes to the pairedNames (a struct that has all nodes)
// and update the levels of the nodes. When a node is updated, it notifies its correlative node to update its level
// with a level + 1. If the subscriber node has a level lesser than the notifier node,
// the subscriber node level is updated.
func (s *Sorter) ReturnOrderedNames(names Names) (OrderedResponse, error) {
	diagram := diagram{
		nodes: make(nodesMap),
	}
	for _, pairedName := range names.PairedNames {
		err := diagram.addNodes(pairedName.SubscriberNodeName, pairedName.NotifierNodeName)
		if err != nil {
			return OrderedResponse{}, err
		}
	}
	return OrderedResponse{
		NamesInfo: diagram.getOrderedNamesInfo(),
	}, nil
}

type diagram struct {
	nodes nodesMap
}

type nodesMap map[string]*node

// node represents a node of the diagram.
type node struct {
	name           string
	level          int
	correlative    *node
	cyclicObserver bool
}

func (d *diagram) getOrderedNamesInfo() []SortedNamesInfo {
	var sortedNamesInfo []SortedNamesInfo
	for _, node := range d.nodes {
		sortedNamesInfo = append(sortedNamesInfo, SortedNamesInfo{
			Name:  node.name,
			Level: node.level,
		})
	}
	// now we need to sort the sortedNamesInfo by Level and Name
	sort.Slice(sortedNamesInfo, func(i, j int) bool {
		if sortedNamesInfo[i].Level == sortedNamesInfo[j].Level {
			return sortedNamesInfo[i].Name < sortedNamesInfo[j].Name
		}
		return sortedNamesInfo[i].Level < sortedNamesInfo[j].Level
	})
	return sortedNamesInfo
}

func (d *diagram) addNodes(subscriberNodeName, notifierNodeName string) error {
	subscriberNode := d.addNodeIfNotExists(subscriberNodeName)
	notifierNode := d.addNodeIfNotExists(notifierNodeName)
	return d.updateNodes(subscriberNode, notifierNode)
}

func (d *diagram) addNodeIfNotExists(name string) *node {
	node, ok := d.nodes[name]
	if !ok {
		node = createNode(name)
		d.nodes[name] = node
	}
	return node
}

func createNode(name string) *node {
	return &node{
		name:        name,
		level:       0,
		correlative: nil,
	}
}

func (d *diagram) updateNodes(subscriberNode, notifierNode *node) error {
	notifierNode.correlative = subscriberNode
	err := notifierNode.updateAndNotifyLevel(notifierNode.level)
	if err != nil {
		return err
	}
	d.cleanCyclicObservers()
	return nil
}

func (n *node) updateAndNotifyLevel(level int) (err error) {
	if n.cyclicObserver {
		return newErrCyclicObserver(n.name)
	}
	n.cyclicObserver = true
	if n.level < level {
		n.level = level
	}
	if n.correlative != nil {
		err = n.correlative.updateAndNotifyLevel(level + 1)
	}
	return
}

func (d *diagram) cleanCyclicObservers() {
	for _, node := range d.nodes {
		node.cyclicObserver = false
	}
}
