package sort

type Names struct {
	PairedNames []PairedNames
}

// PairedNames represents a pair of names that are related to each other.
// If a NotifierNodeName suffer a change, the SubscriberNodeName must be notified.
type PairedNames struct {
	SubscriberNodeName string
	NotifierNodeName   string
}

// OrderedResponse represents the response of the sorter service.
type OrderedResponse struct {
	NamesInfo []SortedNamesInfo
}

// SortedNamesInfo represents the name and level of a ordered node.
type SortedNamesInfo struct {
	Name  string
	Level int
}
