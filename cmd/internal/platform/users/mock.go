package users

import "context"

// UserMock is a mock of the authorize interface.
// TODO: replace this mock with a real implementation.
type UserMock struct{}

// Authorize allows to authorize a user. This is a mocked implementation.
func (u *UserMock) Authorize(_ context.Context, user, password string) bool {
	return user == "admin" && password == "admin"
}
