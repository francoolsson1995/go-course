package config

import (
	"time"

	"gorm.io/gorm/logger"
)

// Config contains in-memory scope configurations and environment secret values.
type Config struct {
	// Scope contains ScopeConfig.
	Scope ScopeConfig
	// Secrets contains Secret.
	Secrets SecretsConfig
}

// ScopeConfig contains in-memory scope configurations.
type ScopeConfig struct {
	Database Database
}

// SecretsConfig contains environment secret values.
type SecretsConfig struct {
	Credentials string
}

// Database contains database configurations.
type Database struct {
	Host               string
	LogLevel           logger.LogLevel
	Name               string
	Password           string
	Port               string
	User               string
	PoolSize           int
	ConnectionLifetime time.Duration
}
