// Package config provides in-memory configurations and environments secrets.
package config

import (
	"os"
	"time"

	"gorm.io/gorm/logger"
)

const (
	authEnv = "AUTH_ENV"

	defaultPoolSize = 15
	defaultLifetime = 270000 * time.Millisecond
)

const (
	// EnvironmentLocal is the tag for local scope.
	EnvironmentLocal = "local"
)

var configs = map[string]ScopeConfig{
	EnvironmentLocal: {
		Database: Database{
			User:               "postgres",
			Password:           "postgres",
			Host:               "127.0.0.1",
			Port:               "5432",
			Name:               "postgres",
			LogLevel:           logger.Info,
			PoolSize:           defaultPoolSize,
			ConnectionLifetime: defaultLifetime,
		},
	},
}

// FromScope returns Config from the current scope.
func FromScope(scope string) (Config, error) {
	if scope == "" {
		scope = EnvironmentLocal
	}
	if scope == EnvironmentLocal {
		err := loadCredentialsToEnv()
		if err != nil {
			return Config{}, err
		}
	}
	scopeConfig, found := configs[scope]
	if !found {
		return Config{}, ErrorScopeConfig
	}
	secretConfigs, err := getSecrets()
	if err != nil {
		return Config{}, err
	}
	return Config{
		Scope:   scopeConfig,
		Secrets: secretConfigs,
	}, nil
}

func loadCredentialsToEnv() error {
	credentials, err := os.ReadFile("cmd/internal/platform/config/firebase-dummy-credencial.json")
	if err != nil {
		return err
	}
	return os.Setenv(authEnv, string(credentials))
}

func getSecrets() (SecretsConfig, error) {
	credentials := os.Getenv(authEnv)
	if credentials == "" {
		return SecretsConfig{}, ErrorSecret
	}
	return SecretsConfig{
		Credentials: credentials,
	}, nil
}
