package config

import "errors"

var (
	// ErrorSecret represents an error when there are empty values in secret field.
	ErrorSecret = errors.New("there are empty values in secret fields")
	// ErrorScopeConfig represents an error when there are no config for current scope.
	ErrorScopeConfig = errors.New("there are not config for current scope")
)
