package course

import (
	"fmt"
)

func newErrCannotSortCourses(err error) error {
	return &ErrCannotSortCourses{err: err}
}

// ErrCannotSortCourses is a struct that represents an error because is imposible to sort courses.
type ErrCannotSortCourses struct {
	err error
}

// Error method allows ErrCannotSortCourses implements error interface.
func (ec *ErrCannotSortCourses) Error() string {
	if unwrapErr := ec.Unwrap(); unwrapErr != nil {
		return fmt.Sprintf("%s - %s", "Cannot sort courses", unwrapErr.Error())
	}
	return "Cannot sort courses"
}

// Is method allows ErrCannotSortCourses implements itself errors assertions.
func (ec *ErrCannotSortCourses) Is(err error) bool {
	_, ok := err.(*ErrCannotSortCourses)
	return ok
}

// Unwrap allows to get a wrapped error in ErrCannotSortCourses.
func (ec *ErrCannotSortCourses) Unwrap() error {
	return ec.err
}

func newErrorInternal(detail string, err error) error {
	return &ErrorInternal{
		processErr: detail,
		err:        err,
	}
}

// ErrorInternal is a struct that represents an unexpected error in the service.
type ErrorInternal struct {
	processErr string
	err        error
}

// Error method allows ErrorInternal implements error interface.
func (ec *ErrorInternal) Error() string {
	if unwrapErr := ec.Unwrap(); unwrapErr != nil {
		return fmt.Sprintf("%s - %s", ec.processErr, unwrapErr.Error())
	}
	return ec.processErr
}

// Is method allows ErrorInternal implements itself errors assertions.
func (ec *ErrorInternal) Is(err error) bool {
	if errProcessing, ok := err.(*ErrorInternal); ok {
		return errProcessing.processErr == ec.processErr
	}
	return false
}

// Unwrap allows to get a wrapped error in ErrorInternal.
func (ec *ErrorInternal) Unwrap() error {
	return ec.err
}
