package course

import (
	"errors"
	"testing"

	"bankuish/cmd/internal/platform/sort"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type mockSorterService struct {
	mock.Mock
}

func (m *mockSorterService) ReturnOrderedNames(_ sort.Names) (sort.OrderedResponse, error) {
	args := m.Called()
	return args.Get(0).(sort.OrderedResponse), args.Error(1)
}

type mockRepository struct {
	mock.Mock
}

func (mr *mockRepository) AddOrderedCourses(_ OrderedCourses) (recordID int, err error) {
	args := mr.Called()
	return args.Int(0), args.Error(1)
}

func (mr *mockRepository) GetByID(_ int) (orderedCourses OrderedCourses, err error) {
	args := mr.Called()
	return args.Get(0).(OrderedCourses), args.Error(1)
}

const (
	physicsTwo = "Physics two"
	physicsOne = "Physics one"
	chemistry  = "Chemistry"
	testUser   = "testUser"
)

var okResponse = OrderedCourses{
	ID:     1,
	UserID: testUser,
	Information: []Info{
		{
			Name:  physicsOne,
			Order: 0,
		},
		{
			Name:  physicsTwo,
			Order: 1,
		},
		{
			Name:  chemistry,
			Order: 2,
		},
	},
}

func TestService_CreateCourseList(t *testing.T) {
	okSortOrderedResponse := sort.OrderedResponse{
		NamesInfo: []sort.SortedNamesInfo{
			{
				Name:  physicsOne,
				Level: 1,
			},
			{
				Name:  physicsTwo,
				Level: 0,
			},
			{
				Name:  chemistry,
				Level: 2,
			},
		},
	}

	someErr := errors.New("some error")

	userCoursesInfoTest := UserCoursesInfo{
		UserID: testUser,
	}

	okSorterService := func() sorterService {
		mockService := mockSorterService{}
		mockService.On("ReturnOrderedNames", mock.Anything).Return(okSortOrderedResponse, nil)
		return &mockService
	}

	okRepository := func() Repository {
		mockRepository := mockRepository{}
		mockRepository.On("AddOrderedCourses", mock.Anything).Return(1, nil)
		mockRepository.On("GetByID", mock.Anything).Return(okResponse, nil)
		return &mockRepository
	}

	tests := []struct {
		name              string
		sorterServiceFunc func() sorterService
		repositoryService func() Repository
		wantResponse      OrderedCourses
		wantErr           error
	}{
		{
			name:              "Happy Path - service work as expected",
			sorterServiceFunc: okSorterService,
			repositoryService: okRepository,
			wantResponse:      okResponse,
		},
		{
			name: "Sad Path - sorter service return an error",
			sorterServiceFunc: func() sorterService {
				mockService := mockSorterService{}
				mockService.On("ReturnOrderedNames", mock.Anything).Return(sort.OrderedResponse{},
					someErr)
				return &mockService
			},
			repositoryService: okRepository,
			wantErr:           newErrCannotSortCourses(someErr),
		},
		{
			name:              "Sad Path - repository service return an error",
			sorterServiceFunc: okSorterService,
			repositoryService: func() Repository {
				mockRepository := mockRepository{}
				mockRepository.On("AddOrderedCourses", mock.Anything).Return(0, someErr)
				return &mockRepository
			},
			wantErr: newErrorInternal("cannot add ordered courses to repository", nil),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.sorterServiceFunc(), tt.repositoryService())
			gotResponse, err := s.CreateCourseList(userCoursesInfoTest)
			require.Equal(t, gotResponse, tt.wantResponse)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}

func TestService_GetCourseList(t *testing.T) {
	tests := []struct {
		name              string
		repositoryService func() Repository
		wantResponse      OrderedCourses
		wantErr           error
	}{
		{
			name: "Happy Path - Get course list by ID works ok",
			repositoryService: func() Repository {
				mockRepository := mockRepository{}
				mockRepository.On("GetByID", mock.Anything).Return(okResponse, nil)
				return &mockRepository
			},
			wantResponse: okResponse,
		},
		{
			name: "Sad Path - Get course list by ID return an unexpected error",
			repositoryService: func() Repository {
				mockRepository := mockRepository{}
				mockRepository.On("GetByID", mock.Anything).Return(OrderedCourses{}, errors.New("some error"))
				return &mockRepository
			},
			wantErr: newErrorInternal("cannot get ordered courses from repository", nil),
		},
		{
			name: "Sad Path - Get course list by ID return not found error",
			repositoryService: func() Repository {
				mockRepository := mockRepository{}
				mockRepository.On("GetByID", mock.Anything).Return(OrderedCourses{}, ErrNotFound)
				return &mockRepository
			},
			wantErr: ErrNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(nil, tt.repositoryService())
			gotResponse, err := s.GetCourseListByID(1)
			require.Equal(t, gotResponse, tt.wantResponse)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}
