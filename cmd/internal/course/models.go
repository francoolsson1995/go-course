package course

// UserCoursesInfo is the information of certain user that wants to take
// some desired courses attached to a certain required course.
type UserCoursesInfo struct {
	UserID  string
	Courses []Dependency
}

// Dependency is the information of a desired course that a user wants to take, with the
// required courses that this desired course has.
type Dependency struct {
	DesiredCourse  string
	RequiredCourse string
}

// OrderedCourses is the responde of the service that contains the ordered courses that a user can take
// related to the required courses.
type OrderedCourses struct {
	ID          int
	UserID      string
	Information []Info
}

// Info is the information of a course that a user can take, with the order that this course must be taken.
type Info struct {
	Name  string
	Order int
}
