package course

import "errors"

// ErrNotFound is the error returned when a course is not found.
var ErrNotFound = errors.New("not found")

// Repository contains the sign to allow a storage to implements it.
// If you want to add a new storage, you must implement this interface.
type Repository interface {
	AddOrderedCourses(orderedCourses OrderedCourses) (recordID int, err error)
	GetByID(recordID int) (orderedCourses OrderedCourses, err error)
}
