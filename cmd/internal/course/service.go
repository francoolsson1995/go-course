package course

import (
	"errors"

	"bankuish/cmd/internal/platform/sort"
)

type sorterService interface {
	ReturnOrderedNames(names sort.Names) (sort.OrderedResponse, error)
}

// Service contains core functionalities to manage courses.
type Service struct {
	sorterService sorterService
	repository    Repository
}

// NewService creates a new course service.
func NewService(sorterService sorterService, repository Repository) *Service {
	return &Service{
		sorterService: sorterService,
		repository:    repository,
	}
}

// CreateCourseList creates a list of courses ordered by dependencies.
func (s *Service) CreateCourseList(userCoursesInfo UserCoursesInfo) (OrderedCourses, error) {
	names := userCoursesInfoToSortNames(userCoursesInfo)
	orderedNames, err := s.sorterService.ReturnOrderedNames(names)
	if err != nil {
		return OrderedCourses{}, newErrCannotSortCourses(err)
	}
	orderedCourses := sortOrderedResponseToOrderedCourses(orderedNames, userCoursesInfo)
	recordID, err := s.repository.AddOrderedCourses(orderedCourses)
	if err != nil {
		return OrderedCourses{}, newErrorInternal("cannot add ordered courses to repository", err)
	}
	orderedCourses.ID = recordID
	return orderedCourses, nil
}

// GetCourseListByID takes an int and returns a course.OrderedCourses.
func (s *Service) GetCourseListByID(recordID int) (OrderedCourses, error) {
	orderedCourses, err := s.repository.GetByID(recordID)
	if err != nil {
		// check if error is not found
		if errors.Is(err, ErrNotFound) {
			return OrderedCourses{}, ErrNotFound
		}
		return OrderedCourses{}, newErrorInternal("cannot get ordered courses from repository", err)
	}
	return orderedCourses, nil
}

func userCoursesInfoToSortNames(userInfo UserCoursesInfo) sort.Names {
	var pairedNames []sort.PairedNames
	for _, dependency := range userInfo.Courses {
		pairedNames = append(pairedNames, sort.PairedNames{
			SubscriberNodeName: dependency.DesiredCourse,
			NotifierNodeName:   dependency.RequiredCourse,
		})
	}
	return sort.Names{
		PairedNames: pairedNames,
	}
}

func sortOrderedResponseToOrderedCourses(orderedResponse sort.OrderedResponse, userInfo UserCoursesInfo) OrderedCourses {
	var info []Info
	for i, orderedName := range orderedResponse.NamesInfo {
		info = append(info, Info{
			Name:  orderedName.Name,
			Order: i,
		})
	}
	return OrderedCourses{
		Information: info,
		UserID:      userInfo.UserID,
	}
}
