package firebase

import (
	"context"

	"bankuish/cmd/internal/platform/config"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"google.golang.org/api/option"
)

// SetupFirebase sets up the firebase app to be used in the app.
func SetupFirebase(config config.Config) (*auth.Client, error) {
	opt := option.WithCredentialsJSON([]byte(config.Secrets.Credentials))
	// Firebase admin SDK initialization
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return nil, err
	}
	// Firebase Auth initialization
	firebaseAuth, err := app.Auth(context.Background())
	if err != nil {
		return nil, err
	}
	return firebaseAuth, nil
}
