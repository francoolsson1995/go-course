package localpostgres

import (
	"encoding/json"
	"errors"

	"bankuish/cmd/internal/course"

	"gorm.io/gorm"
)

type Postgres struct {
	DB *gorm.DB
}

// NewPostgres returns a new postgresImplementation repository.
func NewPostgres(database *gorm.DB) *Postgres {
	return &Postgres{
		DB: database,
	}
}

type audits struct {
	ID       int    `gorm:"column:id;primaryKey"`
	UserID   string `gorm:"column:user_id"`
	JSONInfo string `gorm:"column:json_info"`
}

// AddOrderedCourses tajes a course.OrderedCourses, transforms it to an audits and
// then persists it in the database.
// These methods allow to postgresImplementation to implement the course.Repository interface.
func (p *Postgres) AddOrderedCourses(orderedCourses course.OrderedCourses) (int, error) {
	var jsonInfo json.RawMessage
	jsonInfo, err := json.Marshal(orderedCourses.Information)
	if err != nil {
		return 0, err
	}
	auditsRecord := audits{
		UserID:   orderedCourses.UserID,
		JSONInfo: string(jsonInfo),
	}
	tx := p.DB.Create(&auditsRecord)
	if tx.Error != nil {
		return 0, tx.Error
	}
	return auditsRecord.ID, nil
}

// GetByID takes an int and returns a course.OrderedCourses.
// These methods allow to postgresImplementation to implement the course.Repository interface.
func (p *Postgres) GetByID(recordID int) (course.OrderedCourses, error) {
	var auditsRecord audits
	tx := p.DB.First(&auditsRecord, "id=?", recordID)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return course.OrderedCourses{}, course.ErrNotFound
		}
		return course.OrderedCourses{}, tx.Error
	}
	var jsonInfo []course.Info
	err := json.Unmarshal([]byte(auditsRecord.JSONInfo), &jsonInfo)
	if err != nil {
		return course.OrderedCourses{}, err
	}
	return course.OrderedCourses{
		ID:          recordID,
		UserID:      auditsRecord.UserID,
		Information: jsonInfo,
	}, nil
}
