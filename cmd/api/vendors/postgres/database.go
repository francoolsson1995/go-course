package localpostgres

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bankuish/cmd/internal/platform/config"

	"gorm.io/driver/postgres"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

const connectionRetries = 5

func newGormConnect(cfg config.Database) (*gorm.DB, error) {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{LogLevel: cfg.LogLevel},
	)

	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable", cfg.Host, cfg.User, cfg.Password, cfg.Name, cfg.Port)
	connectionTries := 0
	instance, err := gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})
	for err != nil && connectionTries <= connectionRetries {
		// retry connection every one second until db is connected
		instance, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})
		connectionTries++
		time.Sleep(time.Second)
	}

	if err != nil {
		return nil, fmt.Errorf("an error occurred while open gorm: %w", err)
	}

	instanceDB, err := instance.DB()
	if err != nil {
		return nil, fmt.Errorf("an error occurred getting db instance: %w", err)
	}

	instanceDB.SetMaxIdleConns(cfg.PoolSize)
	instanceDB.SetMaxOpenConns(cfg.PoolSize)
	instanceDB.SetConnMaxLifetime(cfg.ConnectionLifetime)

	if err = applyMigrations(cfg); err != nil && err != migrate.ErrNoChange {
		return nil, fmt.Errorf("an error occurred applying migrations: %w", err)
	}

	return instance, nil
}

func applyMigrations(cfg config.Database) error {
	currentPath := fmt.Sprintf("file:///%s/migrations/", getMainPath())
	url := generateURL(cfg)

	m, err := migrate.New(currentPath, url)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil {
		return err
	}

	return nil
}

func generateURL(cfg config.Database) string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.Name)
}

func getMainPath() string {
	path, err := os.Getwd()
	if err != nil {
		return ""
	}
	idx := strings.LastIndex(path, "/tests")
	if idx < 0 {
		return path
	}
	return path[:idx]
}
