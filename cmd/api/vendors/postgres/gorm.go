package localpostgres

import (
	"bankuish/cmd/internal/platform/config"
	"gorm.io/gorm"
)

// InitGORMConnection returns a new gorm connection.
func InitGORMConnection(cfg config.Config) (*gorm.DB, error) {
	return newGormConnect(cfg.Scope.Database)
}
