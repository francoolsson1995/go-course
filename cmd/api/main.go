package main

import (
	"bankuish/cmd/api/vendors/firebase"
	"bankuish/cmd/api/vendors/postgres"
	"os"

	"bankuish/cmd/api/handlers/middlewares"
	"bankuish/cmd/internal/platform/users"

	"bankuish/cmd/api/handlers"
	"bankuish/cmd/internal/course"
	"bankuish/cmd/internal/platform/config"
	"bankuish/cmd/internal/platform/sort"

	"github.com/gofiber/fiber/v2/middleware/logger"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	log "github.com/sirupsen/logrus"
)

const (
	errConfiguration = iota
	errFirebase
	errPostgres
	errApp
)

const (
	port  = ":8080"
	scope = "SCOPE"
)

func main() {
	// App middlewares
	app := fiber.New(middlewares.GetFiberErrorHandler())
	app.Use(recover.New())
	app.Use(logger.New())

	// Get configs
	cfg, err := config.FromScope(os.Getenv(scope))
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(errConfiguration)
	}

	// firebase auth
	auth, err := firebase.SetupFirebase(cfg)
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(errFirebase)
	}
	app.Use(func(c *fiber.Ctx) error {
		c.Locals(middlewares.AuthKey, auth)
		return c.Next()
	})

	// postgres implementation
	gormConnection, err := localpostgres.InitGORMConnection(cfg)
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(errPostgres)
	}
	postgresService := localpostgres.NewPostgres(gormConnection)

	// Instantiate services
	sorter := sort.NewSorterService()
	courseService := course.NewService(sorter, postgresService)

	// TODO: replace with real users service.
	usersService := &users.UserMock{}

	// Routes
	app = handlers.CoursesRoutes(app, courseService, middlewares.AuthMiddleware())
	app = handlers.AuthenticationRoutes(app, auth, usersService)

	// Ready to go (rocket emoji)
	err = app.Listen(port)
	log.Fatal(err.Error())
	os.Exit(errApp)
}
