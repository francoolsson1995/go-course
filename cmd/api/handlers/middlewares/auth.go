package middlewares

import (
	"net/http"
	"strings"

	"firebase.google.com/go/auth"
	"github.com/gofiber/fiber/v2"
)

const (
	// AuthKey is the key used to store the auth service in the context.
	AuthKey = "auth"
)

// AuthMiddleware is a middleware that checks if the request has a valid token.
// If the token is valid, the middleware will continue the request.
// If the token is not valid, the middleware will return a 401 Unauthorized response.
func AuthMiddleware() fiber.Handler {
	// Return new handler
	return func(c *fiber.Ctx) (err error) {
		// Get auth service from context
		authService := c.Locals(AuthKey).(*auth.Client)

		// Get token from header
		authorizationToken := c.Get("Authorization")
		idToken := strings.TrimSpace(strings.Replace(authorizationToken, "Bearer", "", 1))

		if idToken == "" {
			return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"message": "Unauthorized"})
		}

		// Verify token
		token, err := authService.VerifyIDToken(c.Context(), idToken)
		if err != nil {
			return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"message": "Unauthorized"})
		}
		c.Set("UUID", token.UID)
		return c.Next()
	}
}
