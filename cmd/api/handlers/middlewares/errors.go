package middlewares

import "github.com/gofiber/fiber/v2"

// ErrorResponse is a struct that represents a JSON representation of a error.
type ErrorResponse struct {
	Message string `json:"message"`
}

// GetFiberErrorHandler returns a middleware that implements a custom management of an error.
// This handler wraps a fiber.Error response in a JSON response.
func GetFiberErrorHandler() fiber.Config {
	return fiber.Config{
		// Override default error handler
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			errHandler := fiber.ErrInternalServerError

			// Retrieve the custom status code if it's a fiber.*Error
			if e, ok := err.(*fiber.Error); ok {
				errHandler = e
			}
			return ctx.Status(errHandler.Code).JSON(ErrorResponse{Message: errHandler.Message})
		},
	}
}
