package handlers

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"bankuish/cmd/api/handlers/middlewares"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type mockGetTokenService struct {
	mock.Mock
}

func (m *mockGetTokenService) CustomToken(_ context.Context, _ string) (string, error) {
	args := m.Called()
	return args.Get(0).(string), args.Error(1)
}

type mockUserAuthorizer struct {
	mock.Mock
}

func (m *mockUserAuthorizer) Authorize(_ context.Context, _ string, _ string) bool {
	args := m.Called()
	return args.Get(0).(bool)
}

func TestTokenHandler_GetToken(t *testing.T) {
	tests := []struct {
		name                  string
		funcTokenService      func() getTokenService
		funcAuthorizerService func() userAuthorizer
		userTest              string
		passwordTest          string
		expectedStatus        int
		expectedResponse      string
	}{
		{
			name: "Ok retrieve token",
			funcTokenService: func() getTokenService {
				mockService := &mockGetTokenService{}
				mockService.On("CustomToken", mock.Anything).Return("test_token", nil)
				return mockService
			},
			funcAuthorizerService: func() userAuthorizer {
				mockService := &mockUserAuthorizer{}
				mockService.On("Authorize", mock.Anything).Return(true)
				return mockService
			},
			userTest:         "test_user",
			passwordTest:     "test_password",
			expectedStatus:   http.StatusOK,
			expectedResponse: "{\"token\":\"test_token\"}",
		},
		{
			name: "Fail - password is empty",
			funcTokenService: func() getTokenService {
				mockService := &mockGetTokenService{}
				mockService.On("CustomToken", mock.Anything).Return("test_token", nil)
				return mockService
			},
			funcAuthorizerService: func() userAuthorizer {
				mockService := &mockUserAuthorizer{}
				mockService.On("Authorize", mock.Anything).Return(true)
				return mockService
			},
			userTest:         "test_user",
			expectedStatus:   http.StatusUnauthorized,
			expectedResponse: "{\"message\":\"Unauthorized\"}",
		},
		{
			name: "Fail - user is not valid in users service",
			funcTokenService: func() getTokenService {
				mockService := &mockGetTokenService{}
				mockService.On("CustomToken", mock.Anything).Return("test_token", nil)
				return mockService
			},
			funcAuthorizerService: func() userAuthorizer {
				mockService := &mockUserAuthorizer{}
				mockService.On("Authorize", mock.Anything).Return(false)
				return mockService
			},
			userTest:         "test_user",
			passwordTest:     "test_password",
			expectedStatus:   http.StatusUnauthorized,
			expectedResponse: "{\"message\":\"Unauthorized\"}",
		},
		{
			name: "Fail - error in token service",
			funcTokenService: func() getTokenService {
				mockService := &mockGetTokenService{}
				mockService.On("CustomToken", mock.Anything).Return("test_token", io.EOF)
				return mockService
			},
			funcAuthorizerService: func() userAuthorizer {
				mockService := &mockUserAuthorizer{}
				mockService.On("Authorize", mock.Anything).Return(false)
				return mockService
			},
			userTest:         "test_user",
			passwordTest:     "test_password",
			expectedStatus:   http.StatusUnauthorized,
			expectedResponse: "{\"message\":\"Unauthorized\"}",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := fiber.New(middlewares.GetFiberErrorHandler())
			app = AuthenticationRoutes(app, tt.funcTokenService(), tt.funcAuthorizerService())
			req := httptest.NewRequest(http.MethodGet, authenticationPath, nil)
			req.Header.Set(userHeader, tt.userTest)
			req.Header.Set(passHeader, tt.passwordTest)
			response, err := app.Test(req)
			require.NoError(t, err)
			require.Equal(t, tt.expectedStatus, response.StatusCode)
			bytesResponse, err := io.ReadAll(response.Body)
			require.NoError(t, err)
			require.Equal(t, tt.expectedResponse, string(bytesResponse))
		})
	}
}
