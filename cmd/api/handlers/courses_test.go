package handlers

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"bankuish/cmd/api/handlers/middlewares"

	"bankuish/cmd/internal/course"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type mockCoursesService struct {
	mock.Mock
}

func (m *mockCoursesService) CreateCourseList(_ course.UserCoursesInfo) (course.OrderedCourses, error) {
	args := m.Called()
	return args.Get(0).(course.OrderedCourses), args.Error(1)
}

func (m *mockCoursesService) GetCourseListByID(_ int) (course.OrderedCourses, error) {
	args := m.Called()
	return args.Get(0).(course.OrderedCourses), args.Error(1)
}

var okOrderedCourses = course.OrderedCourses{
	ID:     0,
	UserID: "test-id",
	Information: []course.Info{
		{
			Name:  "test_course_1",
			Order: 1,
		},
		{
			Name:  "test_course_2",
			Order: 2,
		},
		{
			Name:  "test_course_3",
			Order: 3,
		},
	},
}

func TestCoursesHandler_GetOrderedCourses(t *testing.T) {
	okCoursesRequest := userCoursesInfoRequest{
		UserID: "test_user",
		Courses: []dependencies{
			{
				DesiredCourse:  "test_course_1",
				RequiredCourse: "test_course_2",
			},
			{
				DesiredCourse:  "test_course_2",
				RequiredCourse: "test_course_3",
			},
		},
	}

	okMockCourseService := func() coursesService {
		mockService := mockCoursesService{}
		mockService.On("CreateCourseList", mock.Anything).Return(okOrderedCourses, nil)
		return &mockService
	}

	errInternal := errors.New("internal error")

	tests := []struct {
		name              string
		funcCourseService func() coursesService
		request           io.Reader
		expectedStatus    int
		expectedResponse  string
	}{
		{
			name:              "ok - get ordered courses without errors in API",
			funcCourseService: okMockCourseService,
			request: func() io.Reader {
				req, err := json.Marshal(okCoursesRequest)
				require.NoError(t, err)
				return bytes.NewReader(req)
			}(),
			expectedStatus: http.StatusCreated,
			expectedResponse: "{\"id\":0,\"userId\":\"test-id\",\"courses\":[{\"course\":\"test_course_1\",\"o" +
				"rder\":1},{\"course\":\"test_course_2\",\"order\":2},{\"course\":\"test_course_3\",\"order\":3}]}",
		},
		{
			name:              "fail - bad request, must throw http status 400",
			funcCourseService: okMockCourseService,
			request: func() io.Reader {
				return bytes.NewReader([]byte("broken json"))
			}(),
			expectedStatus:   http.StatusBadRequest,
			expectedResponse: "{\"message\":\"cannot unmarshall courses request\"}",
		},
		{
			name: "fail - error making sort of courses, must throw unprocessable entity http status 422",
			funcCourseService: func() coursesService {
				mockService := mockCoursesService{}
				mockService.On("CreateCourseList", mock.Anything).Return(course.OrderedCourses{}, &course.ErrCannotSortCourses{})
				return &mockService
			},
			request: func() io.Reader {
				req, err := json.Marshal(okCoursesRequest)
				require.NoError(t, err)
				return bytes.NewReader(req)
			}(),
			expectedStatus:   http.StatusUnprocessableEntity,
			expectedResponse: "{\"message\":\"get ordered courses handler: Cannot sort courses\"}",
		},
		{
			name: "fail - internal error",
			funcCourseService: func() coursesService {
				mockService := mockCoursesService{}
				mockService.On("CreateCourseList", mock.Anything).Return(course.OrderedCourses{}, errInternal)
				return &mockService
			},
			request: func() io.Reader {
				req, err := json.Marshal(okCoursesRequest)
				require.NoError(t, err)
				return bytes.NewReader(req)
			}(),
			expectedStatus:   http.StatusInternalServerError,
			expectedResponse: "{\"message\":\"Internal Server Error\"}",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := fiber.New(middlewares.GetFiberErrorHandler())
			app = CoursesRoutes(app, tt.funcCourseService(), func(c *fiber.Ctx) error {
				return c.Next()
			})
			req := httptest.NewRequest(http.MethodPost, coursePathGroup+sortPath, tt.request)
			response, err := app.Test(req)
			require.NoError(t, err)
			require.Equal(t, tt.expectedStatus, response.StatusCode)
			bytesResponse, err := io.ReadAll(response.Body)
			require.NoError(t, err)
			require.Equal(t, tt.expectedResponse, string(bytesResponse))
		})
	}
}

func TestCoursesHandler_GetCourseListByID(t *testing.T) {
	okMockCourseService := func() coursesService {
		mockService := mockCoursesService{}
		mockService.On("GetCourseListByID", mock.Anything).Return(okOrderedCourses, nil)
		return &mockService
	}

	errInternal := errors.New("internal error")

	tests := []struct {
		name              string
		funcCourseService func() coursesService
		expectedStatus    int
		expectedResponse  string
	}{
		{
			name:              "ok - get ordered courses without errors in API",
			funcCourseService: okMockCourseService,
			expectedStatus:    http.StatusOK,
			expectedResponse: "{\"id\":0,\"userId\":\"test-id\",\"courses\":[{\"course\":\"test_course" +
				"_1\",\"order\":1},{\"course\":\"test_course_2\",\"order\":2},{\"course\":\"test_course_3\",\"ord" +
				"er\":3}]}",
		},
		{
			name: "fail - internal error",
			funcCourseService: func() coursesService {
				mockService := mockCoursesService{}
				mockService.On("GetCourseListByID", mock.Anything).Return(course.OrderedCourses{}, errInternal)
				return &mockService
			},
			expectedStatus:   http.StatusInternalServerError,
			expectedResponse: "{\"message\":\"Internal Server Error\"}",
		},
		{
			name: "fail - entity not found",
			funcCourseService: func() coursesService {
				mockService := mockCoursesService{}
				mockService.On("GetCourseListByID", mock.Anything).Return(course.OrderedCourses{}, course.ErrNotFound)
				return &mockService
			},
			expectedStatus:   http.StatusNotFound,
			expectedResponse: "{\"message\":\"not found\"}",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := fiber.New(middlewares.GetFiberErrorHandler())
			app = CoursesRoutes(app, tt.funcCourseService(), func(c *fiber.Ctx) error {
				return c.Next()
			})
			req := httptest.NewRequest(http.MethodGet, coursePathGroup+"/1", nil)
			response, err := app.Test(req)
			require.NoError(t, err)
			require.Equal(t, tt.expectedStatus, response.StatusCode)
			bytesResponse, err := io.ReadAll(response.Body)
			require.NoError(t, err)
			require.Equal(t, tt.expectedResponse, string(bytesResponse))
		})
	}
}
