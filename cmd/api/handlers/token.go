package handlers

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

const (
	userHeader = "User"
	passHeader = "Password"
)

type getTokenService interface {
	CustomToken(ctx context.Context, uid string) (string, error)
}

type userAuthorizer interface {
	Authorize(ctx context.Context, user, password string) bool
}

// TokenHandler contains functionalities associated to generate a token for an user.
type TokenHandler struct {
	getTokenService getTokenService
	userAuthorizer  userAuthorizer
}

// NewTokenHandler returns a TokenHandler handler.
func NewTokenHandler(getTokenService getTokenService, userAuthorizer userAuthorizer) *TokenHandler {
	return &TokenHandler{
		getTokenService: getTokenService,
		userAuthorizer:  userAuthorizer,
	}
}

// GetToken generates a token for a user. It validates if user and password exists in
// the users sersuvice, and if so, it generates a token for the user using an authorization service.
func (th *TokenHandler) GetToken(c *fiber.Ctx) error {
	ctx := c.Context()
	user := c.Get(userHeader)
	if user == "" {
		log.Info("empty user attempting to get a token")
		return fiber.ErrUnauthorized
	}
	password := c.Get(passHeader)
	if password == "" {
		log.Info("empty password attempting to get a token")
		return fiber.ErrUnauthorized
	}
	isValid := th.userAuthorizer.Authorize(ctx, user, password)
	if !isValid {
		log.Info("invalid attempt to get a token")
		return fiber.ErrUnauthorized
	}
	token, err := th.getTokenService.CustomToken(ctx, user)
	if err != nil {
		log.Error(fmt.Sprintf("cannot get a token for user %s", user))
		return fiber.ErrUnauthorized
	}
	return c.JSON(tokenResponse{Token: token})
}
