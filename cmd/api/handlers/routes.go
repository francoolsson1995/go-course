package handlers

import (
	"github.com/gofiber/fiber/v2"
)

const (
	coursePathGroup = "/courses"
	sortPath        = "/sort"
	iDPath          = "/:id"

	authenticationPath = "/authentication"
)

// CoursesRoutes returns a fiber.App with the routes for the courses services.
func CoursesRoutes(app *fiber.App, service coursesService, authMiddleware fiber.Handler) *fiber.App {
	serviceHandler := NewCoursesHandler(service)
	courseGroup := app.Group(coursePathGroup, authMiddleware)
	courseGroup.Post(sortPath, serviceHandler.GetOrderedCourses, authMiddleware)
	courseGroup.Get(iDPath, serviceHandler.GetCourseListByID, authMiddleware)
	return app
}

// AuthenticationRoutes returns a fiber.App with the routes for the authentication services.
func AuthenticationRoutes(app *fiber.App, tokenService getTokenService, authorizer userAuthorizer) *fiber.App {
	serviceHandler := NewTokenHandler(tokenService, authorizer)
	app.Get(authenticationPath, serviceHandler.GetToken)
	return app
}
