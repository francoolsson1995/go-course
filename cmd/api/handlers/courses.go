package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"bankuish/cmd/internal/course"

	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

type coursesService interface {
	CreateCourseList(userCoursesInfo course.UserCoursesInfo) (course.OrderedCourses, error)
	GetCourseListByID(recordID int) (course.OrderedCourses, error)
}

// CoursesHandler is the handler of a courses service
type CoursesHandler struct {
	service coursesService
}

// NewCoursesHandler returns an instance of CoursesHandler.
func NewCoursesHandler(service coursesService) *CoursesHandler {
	return &CoursesHandler{service: service}
}

// GetOrderedCourses returns is the API handler that returns the ordered courses for a user.
func (ch *CoursesHandler) GetOrderedCourses(c *fiber.Ctx) error {
	coursesRequest := userCoursesInfoRequest{}
	err := json.Unmarshal(c.Body(), &coursesRequest)
	if err != nil {
		log.Warning(fmt.Sprintf("cannot unmarshall courses request: %s", err.Error()))
		return fiber.NewError(http.StatusBadRequest, "cannot unmarshall courses request")
	}
	response, err := ch.service.CreateCourseList(requestToUserCoursesInfo(coursesRequest))
	if err != nil {
		if sortError, ok := err.(*course.ErrCannotSortCourses); ok {
			messageError := fmt.Sprintf("get ordered courses handler: %s", sortError.Error())
			log.Warning(messageError)
			return fiber.NewError(http.StatusUnprocessableEntity, messageError)
		}
		log.Error(fmt.Sprintf("internal error: %s", err.Error()))
		return fiber.ErrInternalServerError
	}
	return c.Status(http.StatusCreated).JSON(orderedCoursesToResponse(response))
}

// GetCourseListByID returns is the API handler that returns the ordered courses for a user by ID.
func (ch *CoursesHandler) GetCourseListByID(c *fiber.Ctx) error {
	recordID := c.Params("id")
	recordIDInt, err := strconv.Atoi(recordID)
	if err != nil {
		log.Info(fmt.Sprintf("cannot convert record ID to int: %s", err.Error()))
		return fiber.NewError(http.StatusBadRequest, "cannot convert record ID to int")
	}
	response, err := ch.service.GetCourseListByID(recordIDInt)
	if err != nil {
		if errors.Is(err, course.ErrNotFound) {
			return fiber.NewError(http.StatusNotFound, "not found")
		}
		log.Error(fmt.Sprintf("internal error: %s", err.Error()))
		return fiber.ErrInternalServerError
	}
	return c.Status(http.StatusOK).JSON(orderedCoursesToResponse(response))
}

func requestToUserCoursesInfo(request userCoursesInfoRequest) course.UserCoursesInfo {
	var dependencies []course.Dependency
	for _, dependency := range request.Courses {
		dependencies = append(dependencies, course.Dependency{
			DesiredCourse:  dependency.DesiredCourse,
			RequiredCourse: dependency.RequiredCourse,
		})
	}
	return course.UserCoursesInfo{
		UserID:  request.UserID,
		Courses: dependencies,
	}
}

func orderedCoursesToResponse(orderedCourses course.OrderedCourses) orderedCoursesResponse {
	var infoResponse []info
	for _, courseInfo := range orderedCourses.Information {
		infoResponse = append(infoResponse, info{
			Name:  courseInfo.Name,
			Order: courseInfo.Order,
		})
	}
	return orderedCoursesResponse{
		ID:          orderedCourses.ID,
		UserID:      orderedCourses.UserID,
		Information: infoResponse,
	}
}
